import React, { Component } from 'react';
import './Dogadaji.css';

import Zapis from '../Zapis.js';
import Godine from '../Godine.js';
import data from '../data.js';

class Dogadaji extends Component{
  constructor() {
    super();

    this.state = {
      event: data.event,
    };
  }

  render() {
    return(
      <div className = "section section-dogadaji">
        <Zapis kategorija={this.state.event}/>
        <Godine/>
      </div>
    );
  }
}

export default Dogadaji;