import React, { Component } from 'react';
import './GodineZapis.css';
import data from './data.js';

import MyDialog from './Dialog/Dialog.js';



class Zapis extends Component {
  constructor(props) {
    super(props);

    this.state = {
      event: props.kategorija,
      isActive: -1,
    };

    // this.handleClick = this.handleClick.bind(this);
    // this.handleCloseZapis = this.handleCloseZapis.bind(this);
  }

  handleClick = k => {
    this.setState({ isActive: k })
  }


  handleCloseZapis = () => {
    this.setState({ isActive: -1 })
    
  };

  render() {
    return (
      <div className="Zapis">
        {this.state.event.map((event) =>
          (<div onClick={(e) => this.handleClick(event.key, e)} key={event.key} className="paper" style={{ gridColumnStart: (event.godina - 1949), gridColumnEnd: (event.godina - 1948) }}>
            {event.naslov}
          </div>)
        )}
        {
          this.state.isActive !== -1 &&
          <MyDialog
            open={true}
            //eventKey={this.state.isActive}
            onClose={this.handleCloseZapis}
            tekst={this.state.event[this.state.isActive].tekst}
            naslov={this.state.event[this.state.isActive].naslov}
            slika={this.state.event[this.state.isActive].slika}
          />
        }
      </div>
    );
  }
}

export default Zapis;