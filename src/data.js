const data = {
  event: [
    {
      key: 0,
      naslov: "Vijetnamski rat",
      slika: "http://localhost:3000/slike/1_vijetnamski_rat.png",
      godina: 1959,
      kategorija: "dogadaj",
      tekst: "Rat u Vijetnamu (također poznat i pod nazivom Drugi indokineski rat, a u Vijetnamu poznat pod nazivima Rat za otpor protiv Amerike ili jednostavnije Američki rat) bio je oružani sukob u eri tzv. hladnoga rata koji se odvijao na prostorima država Vijetnama, Laosa i Kambodže u razdoblju od 1. studenog 1955., pa sve do pada Sajgona 30. travnja 1975. godine. Taj rat zapravo je nastavak Prvog indokineskog rata (1946. – 1954.), a vodio se između Sjevernog Vijetnama – čiji su saveznici bili tadašnji Sovjetski Savez, Kina i ostale komunističke države – te vlade Južnog Vijetnama – čiji su najjači saveznik bile Sjedinjene Američke Države i druge antikomunističke zemlje."
    }, {
      key: 1,
      naslov: "Sagrađen Berlinski zid",
      slika: "./slike/2_berlinski_zid.jpg",
      godina: 1961,
      kategorija: "dogadaj",
      tekst: "Berlinski zid (njem.: Die Berliner Mauer) je bila barijera dugačka oko 160 km koja je odvajala Zapadni od Istočnog Berlina i okolnog teritorija Njemačke Demokratske Republike. Sagradila ga je istočnonjemačka vlast, čija je propaganda taj objekt nazvala ''antifašističkim obrambenim zidom'' (njem. „antifaschistischer Schutzwall“). Svrha zida je bila onemogućiti bijeg stanovnika iz realsocijalističke Istočne u Zapadnu Njemačku. Počeo se graditi 13. kolovoza 1961. godine i više je puta nadograđivan. Otvoren je za neometan promet 9. studenog 1989., a potom je srušen. Bio je oličenje Hladnog rata."
    }, {
      key: 2,
      naslov: "Atentat - John F Kennedy",
      slika: "./slike/3_John_F_Kennedy.jpg",
      godina: 1963,
      kategorija: "dogadaj",
      tekst: "Kennedy se krajem 1963. godine počeo pripremati za izbornu kampanju 1964. godine, pri čemu je poseban naglasak stavljen na južne države, gdje nije bio tako popularan kao u ostatku zemlje. U tu je svrhu odlučio posjetiti Teksas, te je 22. studenog 1963. godine sletio u Dallas. Dok se u otvorenom automobilu vozio ulicama u društvu supruge i teksaškog guvernera Johna Conallyja, na povorku su ispaljena tri hica iz puške. Jedan je metak pogodio i ranio Conallyja, a dva su pogodila Kennedyja - jedan u vrat, i jedan u glavu. Kennedy je odmah odvezen u bolnicu gdje je nedugo potom proglašen mrtvim. Nekoliko sati kasnije Marinac Lee Harvey Oswald je uhićen, a potom i optužen za atentat. Oswald je tvrdio da je nedužan, ali to nije imao prilike dokazati pred sudom. Dva dana kasnije je u policijskoj stanici pred TV-kamerama ubijen od strane Jacka Rubyja, vlasnika noćnog kluba povezanog s organiziranim kriminalom. Suočen s činjenicom da se detalji ubojstva možda nikada neće razotkriti na sudu, Johsnon - koji je temeljem Ustava postao predsjednik - odobrio je rad specijalnog tijela zvanog Warrenova komisija, kojemu je cilj bio pronaći i objaviti sve vezano uz atentat. Zaključak komisije je bio da je Oswald djelovao sam i ubio predsjednika iz nepoznatih motiva. Međutim, detalji vezani uz atentat i počinitelja su mnoge od samog početka mnoge natjerali da sumnjaju i odbace taj zaključak. Umjesto toga se nude razne alternativne teorije zavjere vezane uz širi krug počinitelja, kojima je svoj doprinos dao i Hollywood, ali i zaključi posebnog kongresnog odbora 1970-ih godina."
    }, {
      key: 3,
      naslov: "Prva transplatacija srca",
      slika: "./slike/4_prva_transplatacija_srca.jpg",
      godina: 1967,
      kategorija: "dogadaj",
      tekst: "Pacijent koji je dobio novo srce je bio Louis Washkansky. Operacija je trajala 9 sati, a donator je bila 25-godišnja Denise Darvall koja je nedavno prije toga preminula u saobraćajnoj nesreći. Proceduru je izveo južnoafrički kardiolog Christiaan Barnard. Iako je Washkansky preminuo 18 dana poslije transplantacije uslijed upale pluća izazvane oslabljenim imunim sistemom, Barnard je operaciju smatrao uspješnom jer je njegovo novo srce bilo u potpunosti stimulisano od njegovog tijela."
    }, {
      key: 4,
      naslov: "Atentat - Martin Luther King Jr.",
      slika: "./slike/5_Martin_Luther_King_Jr.jpg",
      godina: 1968,
      kategorija: "dogadaj",
      tekst: "Martin Luther King Junior (Atlanta, 15. siječnja 1929. - Memphis 4. travnja 1968.), američki baptistički svećenik, aktivist za građanska prava i jedan od najvećih vođa za prava američkog crnačkog stanovništva. U listopadu 1964. godine prima Nobelovu nagradu za mir. Ubijen je 4. travnja 1968. hicem iz snajpera na balkonu hotelske sobe u Memphisu, u saveznoj državi Tennessee, u 39. godini života. Sahrani Martina Luthera Kinga Juniora prisustvovalo je preko 300.000 ljudi koji su mu došli odati posljednju počast. Na nadgrobnoj ploči njegova groba stoje riječi crnačke duhovne pjesme Napokon slobodan."
    }, {
      key: 5,
      naslov: "Apollo 11 sletio na mjesec",
      slika: "./slike/6_apollo_11.png",
      godina: 1969,
      kategorija: "dogadaj",
      tekst: "Apollo 11 je bila svemirska misija koja je 20. srpnja 1969. dovela prve ljude na Mjesec. Svemirski let, kojeg su provele Sjedinjene Države, smatra se velikim postignućem u povijesti istraživanja i predstavlja pobjedu SAD-a u hladnoratovskoj svemirskoj utrci sa Sovjetskim Savezom. Let do mjeseca trajao je 2 sata i 24 minute. Apollo 11, ispunio je viziju predsjednika Johna F. Kennedyja o osvajanju Mjeseca prije Sovjetskog saveza do kraja 1960-ih, koju je izrazio tijekom govora u kongresu 1961. Nakon Apolla 11 još pet misija programa Apollo sletjelo je na Mjesec od 1969. do 1972."
    }, {
      key: 6,
      naslov: "Rođeno prvo dijete iz tube",
      slika: "./slike/7_test_tube_baby.png",
      godina: 1978,
      kategorija: "dogadaj",
      tekst: "Prva osoba rođena umjetnom oplodnjom je Engleskinja Louise Brown rođena 1978. godine. Prva umjetna oplodnja u Hrvatskoj obavljena je 1983. godine u Petrovoj bolnici u Zagrebu. U svijetu je do 2010. godine rođeno 4 milijuna djece izvantjelesnom oplodnjom, a u Hrvatskoj oko 20.000. Umjetna oplodnja je medicinski postupak potpomognute oplodnje jajne stanice, transfer oplođenih zametaka u maternicu, implantacija i razvitak trudnoće. Postoji oko dvadeset tehnika (inseminacija, IVF, ICSI, FET, GIFT, ZIFT, itd.), ali najčešće su inseminacija, IVF (in vitro oplodnja), ICSI (intracitoplazmična injekcija spermija) i FET (engl. frozen embryo transfer, transfer odmrznutog zametka). Umjetna oplodnja prakticira se zbog neplodnosti žene ili muškarca ili kad su oboje neplodni ili kad postoji bojazan prenošenja teške genetske bolesti na dijete."
    }, {
      key: 7,
      naslov: "Srušen Berlinski zid",
      slika: "./slike/8_berlinski_zid.jpg",
      godina: 1989,
      kategorija: "dogadaj",
      tekst: "Počeo se graditi 13. kolovoza 1961. godine i više je puta nadograđivan. Već 9. studenog 1989. godine su ljudi samoinicijativno počeli fizički razbijati dijelove Zida, a 13. studenog je istočnonjemačka vojska počela sa organiziranim uklanjanjem nekih njegovih dijelova. Državljanima DDR-a je bilo odmah omogućeno da prelaze iz Istočnog u Zapadni Berlin; međutim je pogranična služba do 23. studenog 1989. godine još uvijek ograničavala ulazak zapadnonjemačkih državljanima u Istični Berlin. Službeno Ponovno ujedinjenje Njemačke je nastupilo 3. listopada 1990. godine."
    }, {
      key: 8,
      naslov: "Domovinski rat",
      slika: "./slike/9_domovinski_rat.jpg",
      godina: 1991,
      kategorija: "dogadaj",
      tekst: "Domovinski rat bio je obrambeno-osloboditeljski rat za neovisnost i cjelovitost hrvatske države protiv agresije udruženih velikosrpskih snaga – ekstremista u Hrvatskoj, BiH (posebice Republike Srpske), JNA te Srbije i Crne Gore. Domovinskom je ratu prethodila pobuna dijela srpskoga pučanstva u Hrvatskoj – tzv. balvan revolucija – koja je izbila u kolovozu 1990. i uslijed koje su se na više strana dogodili manji oružani incidenti. U ratu je poginulo preko 21.000 ljudi. Prema podatcima Državne revizije za popis i procjenu ratne štete, izravna ratna šteta u Hrvatskoj u razdoblju 1990.–1999. godine iznosila je 236.431.568.000 kuna. Tragičnost hrvatskog obrambeno-oslobodilačkog rata jest u tome što napadač nije imao namjeru samo fizički osvojiti ozemlje. Bît je bila i u potpunom uništenju identiteta Hrvatske i Hrvata - čovjeka, kulture i povijesti."
    }, {
      key: 9,
      naslov: "Bosanski genocid",
      slika: "./slike/10_bosanski_genocid.jpg",
      godina: 1992,
      kategorija: "dogadaj",
      tekst: "Zbog teških zločina tijekom rata u Bosni i Hercegovini, skovan je termin Bosanski genocid. Jedna od prvih međunarodnih organizacija koja ukazuje da se u Bosni i Hercegovini događa genocid nad Bošnjacima je Helsinki Watch koji je o tome govorio u prvom dijelu rata. Međunarodni sud za ratne zločine počinjene na području bivše Jugoslavije i Međunarodni sud pravde potvrdili su da se dogodio genocid tijekom sukoba, ali su ga ograničili samo na područje Srebrenice. Ujedinjeni Narodi u listopadu 1992. godine dobivaju službeni izvještaj koji govori o situaciji u Bosni i Hercegovini. Nakon sukoba, pokrenuti su sudovi za procesuiranje tog zločina. Do danas, sveukupno je 14 osoba osuđeno za genocid na području Bosne. ICTY je donio četiri presude, sud u BiH devet a njemački sudovi jednu."
    }, {
      key: 10,
      naslov: "Smrt Princeze Diane",
      slika: "./slike/11_diana.jpg",
      godina: 1997,
      kategorija: "dogadaj",
      tekst: "Diana, velška princeza (Sandringham, Norfolk, Engleska, 1. srpnja 1961. - Pariz, 31. kolovoza 1997.), znana kao Diana, princeza od Walesa ili Lady Di i prva supruga Charlesa, princa od Walesa. Dana 31. augusta, 1997. godine Diana i njen ljubavnik, Dodi al-Fajed, napustili su pariski hotel Ritz dvadeset minuta nakon ponoći i vraćali se u svoj apartman. Nijedan od četiri putnika u automobilu nije nosio pojas. Vozeći velikom brzinom u želji da izmaknu paparazzima, vozač Henri Paul je izgubio kontrolu nad vozilom i udario u trinaesti stub tunela. Henri i Dodi su poginuli na mjestu nesreće. Jedan od novinara koji su nastavili fotografirati mjesto nesreće izjavio je da je Diana bila pri svjesti, ali da je na njegove riječi mogla odgovoriti jedino treptanjem. Teško ranjena Diana prevezena je u bolnicu Pitié-Salpêtrière, i to tek dva sata nakon nesreće. Uprkos naporima da se voljena princeza spasi, Diana je podlegla teškim ozljedama u 4:00. Jedini koji je preživio nesreću bio je Dodijev tjelohranitelj, Trevor Rees-Jones. Diana, svojevremeno princeza od Velsa i žena od koje se očekivalo da postane kraljica, sahranjena je 6. septembra na otočiću usred jezera Round Oval. Njenu sahranu pratile su 2,5 milijarde ljudi."
    },
  ],
  people: [
    {
      key: 0,
      naslov: "Richard Branson",
      slika: "./slike/12_Richard_Branson.jpg",
      godina: 1950,
      kategorija: "dogadaj",
      tekst: "Sir Richard Charles Nicholas Branson (Shamley Green, 18. srpnja 1950.) je britanski poslovni čovjek čije se bogatstvo procijenjuje na 7.8 milijardi američkih dolara. Vlasnik je branda Virgin koji sadrži 350 kompanija, od kojih je najpoznatija zrakoplovna kompanija Virgin Atlantic."
    }, {
      key: 1,
      naslov: "Bill Gates",
      slika: "./slike/13_Bill_Gates.jpg",
      godina: 1955,
      kategorija: "dogadaj",
      tekst: "William Henry ''Bill'' Gates III. (Seattle, 28. listopada 1955.), je američki poslovni magnat, filantrop, najbogatija osoba na svijetu (provjereno 21. siječnja 2015.), prvi najbogatiji Amerikanac (provjereno 13. veljače 2009.) i direktor Microsofta, softverske tvrtke koju je pokrenuo s Paulom Allenom. Tijekom svoje karijere u Microsoftu, Gates je bio izvršni direktor i šef softverske arhitekture, a ostaje i najveći pojedinačni dioničar sa više od 8 % redovitih dionica. Također je autor ili suautor nekoliko knjiga. Gates je jedan od najpoznatijih poduzetnika u revoluciji osobnog računalstva."
    }, {
      key: 2,
      naslov: "Steve Jobs",
      slika: "./slike/14_Steve_Jobs.jpg",
      godina: 1955,
      kategorija: "dogadaj",
      tekst: "Steve Jobs (rođen kao Steven Paul Jobs; San Francisco, Kalifornija, 24. veljače 1955. - Palo Alto, Kalifornija, 5. listopada 2011.) je bio izvršni direktor, biznismen i dizajner tvrtke Apple Inc., čiji je suosnivač. Bio je jedan je od najutjecajnijih ljudi u svijetu računalne industrije. Neki od poznatijih proizvoda tvrke Apple u čijem je razvijanju sudjelovao su iPhone, iPad, iPod i Mac. Jobsu je 2003. dijagnosticiran tumor gušterače, koji je operiran 30. lipnja 2004. godine. Preminuo je 5. listopada 2011. godine u 56. godini od zastoja disanja."
    }, {
      key: 3,
      naslov: "Michael Jackson",
      slika: "./slike/15_Michael_Jackson.jpg",
      godina: 1958,
      kategorija: "dogadaj",
      tekst: "Michael Joseph Jackson (Gary, Indiana, 29. kolovoza 1958. − Los Angeles, Kalifornija, 25. lipnja 2009.), bio je američki pop glazbenik, čiji je kontroverzni osobni život bio dio pop kulture zadnje četvrtine 20. stoljeća. Rođen je u brojnoj glazbenoj obitelji Jackson i sedmo je od devetoro djece. Debitirao je na profesionalnoj glazbenoj sceni s jedanaest godina kao član sastava Jackson 5. Solo karijeru započeo je 1971. godine. Preminuo je desetak dana prije koncerta u sklopu svoje povratničke turneje u londonskoj O2 areni za koju je bilo rasprodano svih 50 koncerata. Prvi je trebao biti održan 13. srpnja 2009., a završili bi u proljeće 2010. godine."
    }, {
      key: 4,
      naslov: "Barack Obama",
      slika: "./slike/16_Barack_Obama.jpg",
      godina: 1961,
      kategorija: "dogadaj",
      tekst: "Barack Hussein Obama, Jr. (Honolulu, Havaji, SAD, 4. kolovoza 1961.), američki političar (Demokratska stranka), predsjednik SAD-a, na dužnosti od 20. siječnja 2009. pa do 20. siječnja 2017. godine. Bivši senator države Illinois i dobitnik Nobelove nagrade za mir 2009. godine zbog zalaganja za nuklearno razoružanje. Obama je prvi afroamerički predsjednik SAD-a, a u svom mandatu je bio i jedini afroamerički senator i tek peti u povijesti SAD-a."
    }, {
      key: 5,
      naslov: "J.K.Rowling",
      slika: "./slike/17_J_K_Rowling.jpg",
      godina: 1965,
      kategorija: "dogadaj",
      tekst: "Joanne ''Jo'' Rowling (Yate, Gloucestershire, 31. srpnja 1965.), britanska je književnica znana po svojoj fantastičnoj seriji romana Harry Potter. Romani o mladom čarobnjaku dostigli su svjetsku slavu, osvojli brojne književne nagrade te su prodani u više od 500 milijuna primjeraka, a po njima je snimljena serija od osam filmova kojima je Rowling odobrila scenarij i imala određan utjecaj tijekom snimanja."
    }, {
      key: 6,
      naslov: "Lance Armstrong",
      slika: "./slike/18_Lance_Armstrong.jpg",
      godina: 1971,
      kategorija: "dogadaj",
      tekst: "Lance Armstrong je umirovljeni američki biciklist. Sedam puta zaredom (1999.-2005.) je osvajao najveću biciklističku utrku na svijetu Tour de France. Svjetski prvak je bio 1993. i vlasnik bronce s Olimpijskih igara u Sydneyu. 2009. godine se vratio profesionalnom biciklizmu nakon trogodišnje pauze i osvojio 3. mjesto na Tour de France-u. 2010. godine je vozio za momčad Radio Shack, koju je i sam osnovao. U kolovozu 2012. Američka antidopinška agencija, zabranjuje mu daljnje nastupe i oduzima mu sve naslove pobjednika Tour de Francea jer je sve pobjede ostvario pod dopingom."
    }, {
      key: 7,
      naslov: "David Beckham",
      slika: "./slike/19_David_Beckham.jpg",
      godina: 1975,
      kategorija: "dogadaj",
      tekst: "David Beckham (Leytonstone, London, 2. svibnja 1975.) umirovljeni je engleski nogometaš i jedan od najpoznatijih svjetskih sportaša. Zadnji klub za koji je igrao je Paris SG. Beckham je također bivši engleski reprezentativac i bivši kapetan Gordog Albiona, na kojem ga je mjestu naslijedio John Terry. David Beckham nije samo odličan nogometaš (1999. i 2001. je bio drugoplasirani u izboru FIFA-e za nogometaša godine) već je i svjetska modna ikona, jedno od najpoznatijih lica u svijetu i brand za koji se otimaju brojni proizvođači sportske opreme, kozmetike, naočala i drugih proizvoda. Beckham je, zajedno sa svojom suprugom Victorijom, bivšom članicom svojevremeno megapopularne pop-grupe Spice Girls, stalan gost tiskovina i elektronskih medija koji se bave poznatim ljudima te je općenito smatran jednom od najznačajnijih figura svjetskog jet-seta, i kao takav, značajan je dio popularne kulture, s brojnim pojavljivanjima u filmovima i TV emisijama."
    }, {
      key: 8,
      naslov: "Cristiano Ronaldo",
      slika: "./slike/20_Cristiano_Ronaldo.jpg",
      godina: 1985,
      kategorija: "dogadaj",
      tekst: "Cristiano Ronaldo dos Santos Aveiro (Funchal, 5. veljače 1985.), poznatiji kao Cristiano Ronaldo, portugalski nogometaš, član madridskoga Reala. Reprezentativac je i kapetan Portugala te dobitnik Zlatne lopte i FIFA-ine nagrade za nogometaša godine za 2008., 2013. i 2014. godinu. Godine 2008. Ronaldo je s Manchester Unitedom osvojio Ligu prvaka te je proglašen igračem turnira. Portugalski nogometni izbornik objavio je u svibnju 2016. popis za nastup na Europskom prvenstvu u Francuskoj, na kojem se nalazi Ronaldo. Trenutno Cristiano Ronaldo vodi bitku sa Lionelom Messiem a obojica imaju po 5 zlatnih lopti."
    }, {
      key: 9,
      naslov: "Usain Bolt",
      slika: "./slike/21_Usain_Bolt.jpg",
      godina: 1986,
      kategorija: "dogadaj",
      tekst: "Usain Bolt (Trelawny, 21. kolovoza 1986.), sprinter i nogometaš sa Jamajke. Bolt trenutno drži svjetski rekord na 100 metara, koji iznosi 9,58 sekunda, i na 200 metara, koji iznosi 19,19 sekunda.[1] Bolt je prvi športaš koji je na istim Olimpijskim igrama oborio oba svjetska rekorda na kratkim stazama i prvi nakon Carla Lewisa 1984., koji je na istim Olimpijskim igrama osvojio zlato u utrkama na 100 i 200 metara. Bolt također drži svjetski juniorski rekord na 200 metara, a on iznosi 19,93 sekunde. Trenutno je fokusiran na svoju nogometnu karijeru i nastupa u Bundesligaškom klubu Borussia Dortmund. Iako nije službeno, ako pokaže najbolje od sebe, mogao bi i službeno započeti igrati u Borussiji Dortmundu."
    }, {
      key: 10,
      naslov: "Malala Yousafzai",
      slika: "./slike/22_Malala_Yousafzai.jpg",
      godina: 1997,
      kategorija: "dogadaj",
      tekst: "Malala Yousafzai (Mingora, 12. srpnja 1997.) je pakistanska učenica i aktivistica za prava djece i najmlađa dobitnica Nobelove nagrade za mir koju je dobila 2014. godine. Poznata je po svom aktivizmu za prava djevojčica i pravo na obrazovanje. Yousafzai je iz grada Mingore u pokrajini Swat. Bila je žrtva napada vatrenim oružjem u listopadu 2012. godine. Pakistanske vlasti odlučile su otvoriti škole s imenom Malale Yousafzai za siromašnu djecu. Oko 300.000 ljudi potpisalo je peticiju da se Malala Yousafzai nominira za Nobelovu nagradu za mir."
    },
  ],
  znamenitosti: [
    {
      key: 0,
      naslov: "Kapela Notre Dame",
      slika: "./slike/23_Notre_Dame.jpg",
      godina: 1954,
      kategorija: "dogadaj",
      tekst: "Kapela Notre Dame du Haut u Ronchampu (francuski: Chapelle Notre-Dame-du-Haut de Ronchamp), poznata i kao Notre Dame du Haut, dovršena 1954. godine, je jedan od najboljih primjera građevina arhitekta Le Corbusiera i jedna od najvažnijih građevina arhitekture 20. stoljeća."
    }, {
      key: 1,
      naslov: "Guggenheim Muzej",
      slika: "./slike/24_Guggenheim_Muzej.jpg",
      godina: 1959,
      kategorija: "dogadaj",
      tekst: "Muzej Solomona R. Guggenheima, do 1952. godine poznat samo kao Guggenheim, je muzej moderne umjetnosti na Petoj aveniji u New Yorku (SAD), s pogledom na Central Park. Zakladu Solomona R. Guggenheima i muzej apstraktne umjetnosti utemeljio je 1939. god. američki industrijalac i sakupljač umjetnina Solomon R. Guggenheim (Philadelphia, 2. veljače 1861. – Port Washington, 3. rujna 1949.). Muzej u New Yorku je bio prvi od mnogih „Guggenheim muzeja” (engleski: Guggenheim Museum) koje je ova obitelj osnovala širom svijeta. Ostali su: Zbirka Peggy Guggenheim u Veneciji (Italija), Njemački Guggenheim u Berlinu, Muzej Guggenheim u Bilbau (Španjolska), Muzej Guggenheim u Las Vegasu (SAD) i dr."
    }, {
      key: 2,
      naslov: "Gateway Arch",
      slika: "./slike/25_Velika_vrata_zapada.jpg",
      godina: 1965,
      kategorija: "dogadaj",
      tekst: "Gateway Arch (Velika vrata zapada) je spomenik od 192 m u St. Louisu, Missouri, USA. Obložen od nehrđajućeg čelika i izgrađen u obliku ponderiranog luka na kolodvoru, to je najviši svjetski luk, najviši umjetnički spomenik na zapadnoj hemisferi, i najviša dostupna zgrada u Missouriju. Izgrađen kao spomenik zapadnoj ekspanziji Sjedinjenih Država i službeno posvećen ''američkom narodu'', središnji je Nacionalni park Gateway Arch i postao je međunarodno priznati simbol sv. Luja kao popularno turističko odredište. Gateway Arch je dizajnirao finsko-američki arhitekt Eero Saarinen 1947, a gradnja je započela 12. veljače 1963., a dovršena je 28. listopada 1965. za 13 milijuna dolara. Spomenik je otvoren javnosti 10. lipnja 1967. Nalazi se na mjestu osnivanja St. Louisa na zapadnoj obali rijeke Mississippi."
    }, {
      key: 3,
      naslov: "World Trade Center tornjevi",
      slika: "./slike/26_World_trade_center_tornjevi.jpg",
      godina: 1973,
      kategorija: "dogadaj",
      tekst: "Izvorni World Trade Center bio je veliki kompleks od sedam zgrada na Donjem Manhattanu, New Yorku, USA. U njemu su se nalazile znamenitosti Twin Towers, koje su otvorene 4. travnja 1973., a 2001. su bile uništene tijekom napada 11. rujna. U vrijeme njihova završetka, Twin Towers - izvorni svjetski trgovački centar, na 417 metara i 2. Svjetski trgovački centar, na 415,1 metara - bile su najviša zgrada na svijetu."
    }, {
      key: 4,
      naslov: "CN toranj, Toronto",
      slika: "./slike/27_cn_toranj.jpg",
      godina: 1976,
      kategorija: "dogadaj",
      tekst: "CN toranj je zgrada u Torontu u Kanadi. Visok je 553,33 metara. Najvažnija je turistička atrakcija u Torontu i privlači više od dva milijuna međunarodnih posjetitelja svake godine. Kanadska nacionalna željeznička tvrtka dala je izgraditi CN toranj. Posjetitelji mogu vidjeti Toronto iz tornja na dvije razine: na nadmorskoj visini od 330 metara i na visini od 447 metara. Kada je sagrađen bio je najviša zgrada na svijetu i najviši toranj na svijetu. To više nije nakon gradnje Burj Khalife, tornja Canton i Tokyo Skytree-a. Još uvijek je najviša zgrada na zapadnoj hemisferi i jedan od najpoznatijih simbola Kanade."
    }, {
      key: 5,
      naslov: "Xanadu House",
      slika: "./slike/28_Xanadu_house.jpg",
      godina: 1983,
      kategorija: "dogadaj",
      tekst: "Kuće Xanadu bile su niz eksperimentalnih domova sagrađenih kako bi se predstavili primjeri računala i automatizacije u kući u Sjedinjenim Državama. Arhitektonski projekt započeo je 1979., a tijekom ranih 1980-ih godina, u različitim dijelovima SAD-a, izgrađeno je tri kuće: jedna u Kissimmeeu, Florida; Wisconsin Dells, Wisconsin; i Gatlinburg, Tennessee. Kuće su uključivale nove tehnike gradnje i dizajna, a 1980-ih su postale popularne turističke atrakcije."
    }, {
      key: 6,
      naslov: "Petronas tornjevi",
      slika: "./slike/29_Petronas_Tornjevi.jpg",
      godina: 1998,
      kategorija: "dogadaj",
      tekst: "Petronas Twin Towers (ili jednostavno Petronas Towers ili Twin Towers) su dva blizanska nebodera povezana hodnikom. Smještena su u Kuala Lumpuru u glavnom gradu Malezije. Građeni su od 1992. do 1998. godine i bili su najviši na svijetu dok ih nije nadmašio Tajvanski Taipei 101, ali najviši su blizanski neboderi na svijetu odkada su srušeni blizanci ''Svjetskog trgovačkog centra'' u New York Cityu u SAD-u. Sadrže 88. katova koji služe za uredske svrhe. Na hodniku koji spaja dva tornja nalazi se prvi opservatorij, dok je drugi smješten na zadnjem katu tornja broj 2."
    },
  ],
  vazniDatumi: [
    {
      key: 0,
      naslov: "Umro Albert Einstein", // 18. travnja 1995.
      slika: "./slike/30_Albert_Einstein.jpg",
      godina: 1955,
      kategorija: "dogadaj",
      tekst: "Albert Einstein (Ulm, 14. ožujka 1879. - Princeton, 18. travnja 1955.) bio je teorijski fizičar, prema jednom izboru najveći fizičar uopće. Mladost je proveo u Münchenu, Italiji i zatim u Švicarskoj, gdje je (1900.) završio studij na Tehničkoj visokoj školi u Zürichu. Einsteinovo je glavno djelo njegova teorija relativnosti (1916.), koja je ne samo od osnovne važnosti kao temeljni okvir za daljnji razvoj teorijske fizike, već duboko zahvaća i u filozofske koncepcije, napose o prostoru i vremenu, a povrh toga u probleme kozmologije i kozmogonije. Einstein umire u 1:15 poslije ponoći u Princetonskoj bolnici u Princetonu, New Jersey, 18. travnja 1955., u svojoj 76. godini života. Uzrok smrti bilo je unutarnje krvarenje, izazvano prskanjem aorte. Njegova opća teorija gravitacije ostaje tako nedovršena. Jedina osoba prisutna u trenutku njegove smrti bila je jedna medicinska sestra, koja je rekla da je on neposredno pred smrt promrmljao nekoliko riječi na njemačkom koje ona nije razumjela. Bio je kremiran bez ceremonije, istoga dana kad je i umro, u Trentonu, New Jersey, što je bilo u skladu s njegovim željama. Njegov pepeo je potom rasut na otvorenom prostoru."
    }, {
      key: 1,
      naslov: "Prva žena u svemiru", // 16. lipnja 1963.
      slika: "./slike/31_Valentina_Tereškova.jpg",
      godina: 1963,
      kategorija: "dogadaj",
      tekst: "Valentina Vladimirovna Tereškova (Rusija, 6. ožujka 1937.), ruska astronautkinja, prva žena u svemiru, heroj Sovjetskog Saveza. S kozmodroma Bajkonura u današnjem Kazahstanu 16. lipnja 1963. lansirana je u svemir brodom Vostok 6, tada je postala prva žena u svemiru (šesta među sovjetskim kozmonautima). Za vrijeme svoga leta komunicirala je s Vostokom 5 koji se istovremeno nalazio u svemiru i kozmonautom Valerijem Bykovim. Nakon 3 dana i 48 kruženja oko Zemlje Tereškova se izbacila iz broda i padobranom spustila na Zemlju. Njezin let se odvijao tjedan dana prije održavanja moskovske međunarodne konferencije žena na kojoj su joj svoje priznanje odali između ostalih i engleska kraljica Elizabeta II. i sovjetski premijer Nikita Hruščov. Nositeljica je najviših državnih počasti i nagrada."
    }, {
      key: 2,
      naslov: "Ubijen Malcolm X", // 21. veljače 1965.
      slika: "./slike/32_Malcolm_X.jpg",
      godina: 1965,
      kategorija: "dogadaj",
      tekst: "Malcolm Little ili Malcolm X (Omaha, Nebraska, 19. svibnja 1925. - Manhattan, New York, 21. veljače 1965.) bio je borac za prava crnaca. Godine 1965. Malcolm je smrtno ranjen dok je držao govor u New Yorku u kojem je kritizirao Elijah Muhammada. Malcolma su ubila tri muškarca za koje se ispostavilo da su članovi Nacije islama. Sva trojica su osuđena i zatvorena."
    }, {
      key: 3,
      naslov: "Održane prve zimske olimpijske igre u Aziji", // 03. veljače 1972.
      slika: "./slike/33_zimske_olimp_igre.gif",
      godina: 1972,
      kategorija: "dogadaj",
      tekst: "XI. Zimske olimpijske igre su održane 1972. godine u gradu Sapporu u Japanu. Bio je to prvi puta da su Zimske olimpijske igre održane u Aziji, odnosno čak i prve ZOI održane izvan Europe ili SAD-a."
    }, {
      key: 4,
      naslov: "Osnovan Microsoft", // 4. travnja 1975.
      slika: "./slike/34_microsoft.png",
      godina: 1975,
      kategorija: "dogadaj",
      tekst: "Microsoft je američka softverska tvrtka. Osnovali su je 1975. godine Bill Gates i Paul Allen. Osnovna djelatnost tvrtke je razvoj osnovnog računalnog softvera poput operacijskih sustava, razvojnih alata, uredskih programa, baza podataka, itd. Microsoft danas drži znatan dio tržišta operacijskih sustava, internetskih pretraživača, uredskog softvera i razvojnih alata. Na tržištu operacijskih sustava namijenjenim osobnim računalima, Windows drži daleko najveći dio tržišnog udjela. Na tržištu operacijskih sustava namijenjenih poslužiteljima (serveri), Microsoft je pak suočen sa snažnom konkurencijom drugih operacijskih sustava. Microsoftovog internetski pretraživač Internet Explorer raširen je prije svega zato što dolazi upakiran u novije verzije Windows operacijskih sustava. Takva vrsta isporuke rezultirala je optužbom Microsofta za monopolizam na američkom sudu. Microsoft je dominantan i na tržištu uredskih programa sa svojim paketom Microsoft Office."
    }, {
      key: 5,
      naslov: "Let Concordea", // 21. siječnja 1976.
      slika: "./slike/35_Concorde.jpg",
      godina: 1976,
      kategorija: "dogadaj",
      tekst: "Concorde je turboreaktivni nadzvučni putnički zrakoplov (supersonic transport SST). Bio je rezultat anglo-francuskog dogovora na razini vlada, te zajednički proizvod Aérospatialea i British Aircraft Corporationa. Nakon prvog leta 1969., uveden je u službu 1976. godine, a povučen je iz prometa 2003., nakon 27 godina službe. Concorde je redovno letio na transatlantskim linijama s londonskog aerodroma Heathrow (British Airways) i pariškog Charles de Gaulle (Air France) za njujorški JFK i washingtonski Dulles, rentabilno na rekordnim brzinama, za manje od polovice vremena potrebnog standardnim putničkim zrakoplovima. Redovni letovi započeli su 21. siječnja, 1976. na rutama London-Bahrain i Paris-Rio de Janeiro (preko Dakara). Kongres Sjedinjenih Država zabranio je slijetanje Concordeu u SAD, uglavnom iz protesta građana radi nadzvučnog buma, što je spriječilo pokretanje željene transatlantske rute. Ipak, američki savezni tajnik za transport, William Coleman, dodijelio je Concordeu posebnu dozovolu za slijetanje na washingtonski aerodrom Dulles International, te su Air France i British Airways simultano započeli službu za Dulles 24. svibnja, 1976."
    }, {
      key: 6,
      naslov: "Stvoren Tetris", // 6. lipnja 1984.
      slika: "./slike/36_tetris.png",
      godina: 1984,
      kategorija: "dogadaj",
      tekst: "Tetris logička je videoigra, izdana na gotovo svim igraćim platformama. Jedna je od najpoznatijih videoigara uopće. Tvorac igre je, u lipnju 1984. bio ruski znanstvenik Aleksej Pažitnov. Pažitnov je radio kao programer u računalnom centru Sovjetske akademije znanosti. Ime ''tetris'' dolazi od grčkog prefiksa''tetra-'' (''četiri''), jer su svi dijelovi sastavljeni od četiri segmenta, i tenisa, Pažitnovljevog omiljenog sporta. Verziju igre za IBM PC platformu napravili su Pažitnovljevi prijatelji Dmitrij Pavlovski i Vadim Gerasimov. Igra (ili njeni brojni klonovi) se pojavila na gotovo svim igraćim konzolama, računalnim sustavima, kao i na uređajima kao što su džepni kalkulatori, mobilni telefoni, džepna računala i drugi. Tetris se čak pojavio kao dio umjetničke izložbe na sveučilištu Brown u SAD-u. Također je 2005. zauzeo treće mjesto u izboru ''100 najvećih videoigara svih vremena'' portala IGN."
    }, {
      key: 7,
      naslov: "Pad Vukovara", // 18. studenog 1991.
      slika: "./slike/37_Vukovar.jpg",
      godina: 1991,
      kategorija: "dogadaj",
      tekst: "Vukovar je grad i najveća hrvatska riječna luka na Dunavu, u hrvatskom dijelu Srijema. On je i upravno, obrazovno, gospodarsko i kulturno središte Vukovarsko-srijemske županije. Osamostaljenjem Hrvatske 1991. godine došlo je do otvorene agresije Srbije na Vukovar i Hrvatsku. Nakon što je većina srpskog stanovništva (sva srpska djeca) pobjegla iz grada, počela je bitka za Vukovar, u kojoj su hrvatske snage branile grad protiv vojske JNA, srpskih četničkih i drugih paravojnih postrojba i srpske vojske, koja je imala golemu premoć u ljudstvu i tehnici. Srpsko je granatiranje sravnilo grad sa zemljom. Nakon tri mjeseca ogorčenih borba, Vukovar je 18. studenoga 1991. godine pao u srpske ruke. U agresiji na Vukovar 1991. godine, JNA i srpske paravojne postrojbe ubile su najmanje 1.739 osoba. Nakon oslobodilačkih akcija u drugim krajevima Hrvatske 1995. godine, počeli su pregovori o povratku Vukovara pod hrvatsku vlast. Lokalna je samouprava u Vukovaru počela djelovati sredinom 1997. godine. Hrvatsko Podunavlje je mirno reintegrirano u Republiku Hrvatsku 15. siječnja 1998. godine. Otad se mnogo radi na obnovi grada i povratku svih stanovnika i pomirbi, te se oživljavaju kulturni i drugi vidovi gradskog života."
    }, {
      key: 8,
      naslov: "Oluja", // 05. kolovoza 1995.
      slika: "./slike/38_Oluja.jpg",
      godina: 1995,
      kategorija: "dogadaj",
      tekst: "Operacija Oluja (4. kolovoza − 7. kolovoza 1995.) je velika vojno-redarstvena operacija u kojoj su Hrvatska vojska i policija oslobodile okupirana područja Republike Hrvatske pod nadzorom pobunjenih Srba, na kojima je bila uspostavljena paradržava Republika Srpska Krajina bez ikakvog međunarodnog priznanja. Operacijom je vraćen u hrvatski ustavno-pravni poredak cijeli okupirani teritorij osim istočne Slavonije. Oluja je, uz operaciju Bljesak, ključna akcija koja je dovela do kraja Domovinskog rata. U operaciji je oslobođeno 10.400 četvornih kilometara ili 18,4 posto ukupne površine Hrvatske."
    }, {
      key: 9,
      naslov: "Objavljeno rođenje ovce Dolly", // 22. veljače 1997.
      slika: "./slike/39_Dolly.jpg",
      godina: 1997,
      kategorija: "dogadaj",
      tekst: "Ovca Dolly (5. srpnja 1996. – 14.veljače 2003.) bila je prvi sisavac koji je uspješno kloniran iz odrasle somatske stanice. Klonirana je na Institutu Roslin u Midlothianu u Škotskoj. Uginula je u dobi od šest godina. Njeno rođenje je objavljeno 22. veljače 1997. Dolly je ispočetka imala šifrirano ime ''6LL3''. Ime ''Dolly'' dobila je u čast Dolly Parton, s obzirom da je klonirana stanica bila dio mliječne žlijezde. Tehnika koju je ovjekovječilo njeno rođenje je somatski stanični nuklearni transfer u kojem se stanica stavlja u ovum bez jezgre, a potom dvije stanice spajaju i razvijaju u embrij. Kada je Dolly klonirana, postala je predmetom kontroverzi koje traju do današnjih dana. 9. travnja 2003. njeni su preparirani ostaci izloženi u Škotskom Nacionalnom muzeju u Edinburghu."
    }, {
      key: 10,
      naslov: "S emitiranjem započela Nova TV", // 28. svibnja 2000.
      slika: "./slike/40_Nova_TV.gif",
      godina: 2000,
      kategorija: "dogadaj",
      tekst: "Nova TV je prva hrvatska komercijalna televizija s nacionalnom koncesijom. S emitiranjem je počela 2000. godine. Od 2004. godine u vlasništvu je medijske korporacije CME Media Enterprises B.V, vodeće medijske grupacije u centralnoj i istočnoj Europi. Prodajom stopostotnog udjela u firmi TotalTV, formalno je maknuta prepreka pri preuzimanju vlasništva Nove TV na ime grupacije United Group koja je vlasnik televizijskih kanala N1 i SportKlub. Danas Nova TV d.d. na hrvatskom tržištu djeluje kao medijska grupa s kanalima Nova TV, specijaliziranim kanalom Doma TV, internacionalnim kanalom Nova World te dječjim kanalom Mini TV."
    },
  ],
}

export default data;