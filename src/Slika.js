import React, { Component } from 'react';
import logo from './logo.svg';

class Slika extends Component {
  render() {
    return (
        <div className="Slika">
          <img src={logo} className="App-logo" alt="logo" />
        </div>
    );
  }
}

export default Slika;