import React, { Component } from 'react';
import './Ljudi.css';

import Zapis from '../Zapis.js';
import Godine from '../Godine.js';
import data from '../data.js';

class Ljudi extends Component{
  constructor() {
    super();

    this.state = {
      people: data.people,
    };
  }

  render() {
    return(
      <div className = "section section-ljudi">
        <Zapis kategorija={this.state.people}/>
        <Godine/>
      </div>
    );
  }
}

export default Ljudi;