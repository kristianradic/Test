import React, { Component } from 'react';
import './Header.css';

import data from '../data.js';

class Header extends Component {
  render() {
    return (
      <div className="Header">
        <div className="kategorija"><h3 className="header__title">Događaji</h3></div>
        <div className="kategorija"><h3 className="header__title">Ljudi</h3></div>
        <div className="kategorija"><h3 className="header__title">Znamenitosti</h3></div>
        <div className="kategorija"><h3 className="header__title">Važni datumi</h3></div>
      </div>
    );
  }
}

export default Header;