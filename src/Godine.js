import React, { Component } from 'react';
import './GodineZapis.css';

class Godine extends Component{
  constructor(){
    super();

    let godinaBuff = [];

    for(let i = 0; i <= 50; i++)
      godinaBuff.push(1950 + i);

    this.state = {
      godina: godinaBuff,
    };
  }


  createContainer = () => {
    let container = this.state.godina.map((godina,index) => <div key={index} className="container"><span className="godine__broj">{godina}</span></div>);

    return container;
  }

  render() {
    return(
      <div className = "Godine">
        {this.createContainer()}
      </div>
    );
  }
}

export default Godine;