import React, { Component } from 'react';
import './Znamenitosti.css';

import Zapis from '../Zapis.js';
import Godine from '../Godine.js';
import data from '../data.js';

class Znamenitosti extends Component{
  constructor() {
    super();

    this.state = {
      znamenitosti: data.znamenitosti,
    };
  }

  render() {
    return(
      <div className = "section section-znamenitosti">
        <Zapis kategorija={this.state.znamenitosti}/>
        <Godine/>
      </div>
    );
  }
}

export default Znamenitosti;