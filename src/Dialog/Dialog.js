import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


class MyDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: props.open,
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
    this.props.onClose();
  };


  render() {
    return (
      <div className="MyDialog">
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{this.props.eventKey}</DialogTitle>
          <DialogContent>
            <h1>{this.props.naslov}</h1>
            {console.log(this.props.naslov)}
            <img data-src="holder.js/500x500/auto" alt="300x300" src={this.props.slika} data-holder-rendered="true"/>
            <DialogContentText id="alert-dialog-description">
                {this.props.tekst}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Zatvori
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default MyDialog;