import React, { Component } from 'react';
import './VazniDatumi.css';

import Zapis from '../Zapis.js';
import Godine from '../Godine.js';
import data from '../data.js';

class VazniDatumi extends Component{
  constructor() {
    super();

    this.state = {
      vazniDatumi: data.vazniDatumi,
      isActive: false,
    };

  }
  render() {
    return(
      <div className = "section section-vazni__datumi">
        <Zapis kategorija={this.state.vazniDatumi}/>
        <Godine/>
      </div>
    );
  }
}

export default VazniDatumi;