import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Dogadaji from './Dogadaji/Dogadaji.js';
import Ljudi from './Ljudi/Ljudi.js';
import Znamenitosti from './Znamenitosti/Znamenitosti.js';
import VazniDatumi from './VazniDatumi/VazniDatumi.js';
import Header from './Header/index.js';

import { createMuiTheme } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import pink from '@material-ui/core/colors/pink';
import red from '@material-ui/core/colors/red';

// All the following keys are optional.
// We try our best to provide a great default value.
const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: pink,
    error: red,
    // Used by `getContrastText()` to maximize the contrast between the background and
    // the text.
    contrastThreshold: 3,
    // Used to shift a color's luminance by approximately
    // two indexes within its tonal palette.
    // E.g., shift from Red 500 to Red 300 or Red 700.
    tonalOffset: 0.2,
  },
});

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <div className="content">
          <Dogadaji/>
          <Ljudi/>
          <Znamenitosti/>
          <VazniDatumi/>
        </div>
      </div>
    );
  }
}


export default App;
